<?php
define("DB_HOST","localhost");
define("DB_NAME","techno-hope");
define("DB_USER","root");
define("DB_PASS","");

function dbConnect()
	{
		$db = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		// echo "<pre>" .print_r($db,true) . "</pre>";
		if (mysqli_connect_errno($db) > 0)
			die ("Connection Fail");
		else
			return $db;
	}
?>